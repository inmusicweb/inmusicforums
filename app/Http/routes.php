<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// This app is being set up as a forum that can be customized to each inMusic brand

Route::get('/', ['middleware' => 'auth', function () {
	return redirect('forum');
}]);

Route::get('/phpinfo', function () {
    echo phpinfo();
    return;
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/users', 'UsersController@index');
Route::get('/users/{id}/edit', 'UsersController@edit');
Route::patch('/users/{id}/edit', 'UsersController@update');

Route::get('/grav', ['middleware' => 'administrator', function(){
	$email="";
	
	if(Auth::user()){
		$email = Auth::user()->email;
	}
	
	$default = 'https://www.gravatar.com/avatar/00000000000000000000000000000000';
	$size = 80;

	$grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;

	return '<a href="https://en.gravatar.com/"><img src="'.$grav_url.'"></a>';

}]);
