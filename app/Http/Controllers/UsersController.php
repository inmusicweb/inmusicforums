<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class UsersController extends Controller
{
    /**
     * Create a new users controller instance.
     */
    public function __construct()
    {
        $this->middleware('administrator');
    }

    /**
     * Show table of all users.
     *
     * @return \View
     */
    public function index()
    {
    	$users = User::get();

    	return view('users.index', compact('users'));
    }

    /**
     * Display form to edit users.
     *
     * @return \View
     */
    public function edit($id)
    {
    	$user = User::findOrFail($id);    	    
    	return view('users.edit', compact('user'));
    }

    /**
     * Update a user record.
     *
     * @return \Redirect
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'role' => 'required',
            ]);

    	$user = User::findOrFail($id); 
        $user->name = $request->name; 
        $user->email = $request->email;
        $user->role = $request->role; 

        $user->save();

    	return redirect('/users');
    }
}
