@extends('layouts.app')

@section('content')

<div class='container'>
	<h3>Users</h3>
	<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
		@foreach($users as $user)
			<tr>
				<td>{{$user->name}}</td>
				<td>{{$user->email}}</td>
				<td>{{$user->role}}</td>
				<td><a class="btn btn-primary btn-sm" href="/users/{{$user->id}}/edit" role="button">Edit User</a></td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>
@endsection