@extends('layouts.app')

@section('content')

<div class='container'>
	<h3>Update User</h3>

	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

	{!! Form::model($user, ['url' => 'users/' . $user->id . '/edit', 'method' => 'PATCH']) !!}
		<div class="form-group">
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::text('email', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{{ Form::select('role', array(
				1 => 'Admin',
				2 => 'Moderator',
				3 => 'User'), null, ['class' => 'form-control'])}}
		</div>

		<div class="form-group">
			{!! Form::submit('Update User', ['class' => 'btn btn-primary btn-sm']) !!}
		</div>

	{!! Form::close() !!}

</div>
@endsection