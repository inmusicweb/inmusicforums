{{-- $category is passed as NULL to the master layout view to prevent it from showing in the breadcrumbs --}}
@extends ('forum::master', ['category' => null])

@section ('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>{{ trans('forum::general.index') }}</h2></div>

                <div class="panel-body">

    @can ('createCategories')
        @include ('forum::category.partials.form-create')
    @endcan

    

                    @foreach ($categories as $category)
                        <table class="table table-index">
                            <thead>
                                <tr>
                                    <th>{{ trans_choice('forum::categories.category', 1) }}</th>
                                    <th class="col-md-2">{{ trans_choice('forum::threads.thread', 2) }}</th>
                                    <th class="col-md-2">{{ trans_choice('forum::posts.post', 2) }}</th>
                                    <th class="col-md-2">{{ trans('forum::threads.newest') }}</th>
                                    <th class="col-md-2">{{ trans('forum::posts.last') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @include ('forum::category.partials.list', ['titleClass' => 'lead'])
                                </tr>
                                @if (!$category->children->isEmpty())
                                    <tr>
                                        <th colspan="5">{{ trans('forum::categories.subcategories') }}</th>
                                    </tr>
                                    @foreach ($category->children as $subcategory)
                                        @include ('forum::category.partials.list', ['category' => $subcategory])
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
